The back-end server and web front-end for my Hot Wheels Collector app.

The server provides an API for:

 - Retrieving collections.
 - Adding and removing cars from a collection.
 - Searching for cars by name, toy number, SKU, etc.
 - Finding car by QR code.
 - Adding custom cars.

The server also:

 - Maintains an up-to-date database of cars from Hot Wheels' website.
 - Caches and cleans-up car images by auto-cropping borders.

The font-end website allows the user to:

 - View their collection.
 - Add and remove cars from their collection.
 - Search for cars. 

This is a very efficient server that doesn’t only allow services to back end or premium users but front end users can also easily view all catalogs of alloy and steel wheels. On the other hand, http://www.2cravelife.com also contributed a lot in this regard to provide a stylish and versatile collection as well. However, front end users can also get the list of cars according to their driving style and need as well at there. 


---------------------------------------

 - [View Website](http://hotwheelscollector.awesomebox.net/)

---------------------------------------

[![Screenshot](https://s3.amazonaws.com/mike-projects/Hot+Wheels+Collector/HotWheelsCollectorWebsiteSC.png)](https://s3.amazonaws.com/mike-projects/Hot+Wheels+Collector/HotWheelsCollectorWebsiteSC.png)